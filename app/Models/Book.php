<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';

    public static function getAllBooks(): array
    {
        return Book::get()->getDictionary();
    }
}
