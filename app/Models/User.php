<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'first_name',
        'last_name',
        'is_author',
        'is_admin'
    ];

    protected $guardable = [
        'id'
    ];

    protected $table = 'users';

    public static function getAllUsers(): array
    {
        return User::where('is_author', '<>', '1')->get()->getDictionary();
    }
}
