<?php
/**
 * Created by PhpStorm.
 * User: ep1demic
 * Date: 11.08.17
 * Time: 19:10
 */

namespace App\Types;


class Redirect
{
    private $from;
    private $to;
    private $message;
    private $code;

    public function __construct($from, $to, $message, $code)
    {
        $this->from = $from;
        $this->to = $to;
        $this->message = $message;
        $this->code = $code;
    }

    public function toJson()
    {
        return json_encode([
            'from' => $this->from,
            'to' => $this->to,
            'message' => $this->message,
            'code' => $this->code
        ], JSON_UNESCAPED_UNICODE);
    }

}