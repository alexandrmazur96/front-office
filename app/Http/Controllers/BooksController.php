<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use App\Models\Book;

class BooksController extends Controller
{
    public function allBooks()
    {
        $currentUser = Session::get('user');
        $books['books'] = Book::getAllBooks();

        if ($currentUser === null) {
            $currentUser = [
                'name_first' => null,
                'name_last' => null,
                'session-id' => null,
                'email' => null
            ];
        }

        $data = $currentUser + $books;

        return view('allBooks', $data);
    }

    public function currentUserBooks()
    {
        $currentUser = Session::get('user');

        $client = new Client(['base_uri' => 'http://api.cabinet.ks']);

        $response = $client->request('GET',
            'api/v1/book', [
                'http_errors' => false,
                'headers' => [
                    'x-session-id' => $currentUser['session_id']
                ]
            ]);
        switch ($response->getStatusCode()) {
            case 200:
                $books = json_decode($response->getBody()->getContents(), true);
                return view('currentUserBooks', $currentUser)
                    ->with('hasBooks', true)
                    ->with('books', $books);
                break;
            case 401:
                return view('currentUserBooks', $currentUser)
                    ->with('hasBooks', false)
                    ->with('error', 'Необходима авторизация!');
                break;
            case 403:
                return view('currentUserBooks', $currentUser)
                    ->with('hasBooks', false)
                    ->with('error', 'Доступ запрещен!');
                break;
            case 404:
                return view('currentUserBooks', $currentUser)
                    ->with('hasBooks', false)
                    ->with('error', 'Добавьте книги для просмотра!');
                break;
        }
    }

    public function currentBook($bookId)
    {
        $currentUser = Session::get('user');
        $client = new Client(['base_uri' => 'http://api.cabinet.ks']);
        $bookResponse = $client->request('GET',
            '/api/v1/book/' . $bookId,
            ['http_errors' => false]);

        if ($bookResponse->getStatusCode() !== 404)
            $bookData = json_decode($bookResponse->getBody()->getContents(), true);

        if ($currentUser === null) {
            $currentUser = [
                'name_first' => null,
                'name_last' => null,
                'session-id' => null,
                'email' => null
            ];
        }

        $data = $bookData + $currentUser;

        return view('currentBook', $data);
    }

    public function sideUserBooks($userId)
    {

    }

    public function searchBooks($term)
    {
        $currentUser = Session::get('user');
        if ($currentUser === null) {
            $currentUser = [
                'name_first' => null,
                'name_last' => null,
                'session-id' => null,
                'email' => null
            ];
        }

        $client = new Client(['base_uri' => 'http://api.cabinet.ks']);

        $response = $client->request('GET',
            'api/v1/book/search/' . $term,
            [
                'http_errors' => false
            ]);

        switch ($response->getStatusCode()) {
            case 200:
                $finded = json_decode($response->getBody()->getContents(), true);
                return view('searchBooks', $currentUser)->with('finded', $finded)->with('error', null);
                break;
            case 400:
            case 404:
                $error = $response->getBody()->getContents();
                return view('searchBooks', $currentUser)->with('error', $error);
                break;
        }
    }

    public function authorBooks($authorId)
    {

    }

    public function addCurrentBook(Request $request, $bookId)
    {
        $currentUser = Session::get('user');

        $client = new Client(['base_uri' => 'http://api.cabinet.ks']);

        $response = $client->request('POST',
            '/api/v1/book/add-to-user',
            [
                'headers' => [
                    'x-session-id' => $currentUser['session_id'],
                    'x-book-id' => $bookId
                ],
                'http_errors' => false
            ]);

        switch ($response->getStatusCode()) {
            case 200:
                Session::put('redirect', [
                    'from' => $_SERVER['HTTP_REFERER'],
                    'to' => $_SERVER['HTTP_REFERER'],
                    'code' => 200,
                    'message' => 'Книга добавлена!'
                ]);
                header('Location: /redirect');
                break;
            case 401:
                Session::put('redirect', [
                    'from' => $_SERVER['HTTP_REFERER'],
                    'to' => $_SERVER['HTTP_REFERER'],
                    'code' => 401,
                    'message' => 'Вы не вошли!'
                ]);
                header('Location: /redirect');
                break;
            case 403:
                Session::put('redirect', [
                    'from' => $_SERVER['HTTP_REFERER'],
                    'to' => $_SERVER['HTTP_REFERER'],
                    'code' => 403,
                    'message' => 'Доступ запрещен!'
                ]);
                header('Location: /redirect');
                break;
            case 404:
                Session::put('redirect', [
                    'from' => $_SERVER['HTTP_REFERER'],
                    'to' => $_SERVER['HTTP_REFERER'],
                    'code' => 200,
                    'message' => 'Книга не найдена!'
                ]);
                header('Location: /redirect');
                break;
        }
    }
}
