<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use App\Models\User;

class UsersController extends Controller
{
    public function allUsers()
    {
        $users['users'] = User::getAllUsers();
        $currentUser = Session::get('user');

        if ($currentUser === null) {
            $currentUser = [
                'name_first' => null,
                'name_last' => null,
                'session_id' => null,
                'email' => null
            ];
        }
        $data = $users + $currentUser;
        return view('allUsers', $data);
    }

    public function currentUser()
    {
        $user = Session::get('user');

        if ($user === null) {
            $user = [
                'name_first' => null,
                'name_last' => null,
                'session_id' => null,
                'email' => null,
            ];

            return view('currentUser', $user);
        }

        $client = new Client(['base_uri' => 'http://api.cabinet.ks']);
        $currentUser['currentUser'] = json_decode($client->request('GET',
            '/api/v1/user/me', [
                'http_errors' => false,
                'headers' => [
                    'X-SESSION-ID' => $user['session_id']
                ]
            ])->getBody()->getContents(), true);
        $data = $user + $currentUser;

        return view('currentUser', $data);
    }

    public function sideUser($userId)
    {
        $data = Session::get('user');
        if ($data === null) {
            $data = [
                'name_first' => null,
                'name_last' => null,
                'session_id' => null,
                'email' => null
            ];
            return view('sideUser', $data);
        }

        $client = new Client(['base_uri' => 'http://api.cabinet.ks']);
        $user['user'] = json_decode($client->request('GET',
            'api/v1/user/' . $userId, [
                'http_errors' => false,
                'headers' => [
                    'X-SESSION-ID' => $data['session_id']
                ]
            ])->getBody()->getContents(), true);

        return view('sideUser', $data + $user);
    }

    public function requestPassword()
    {
        $data = Session::get('user');
        if ($data === null)
            $data = [
                'name_first' => null,
                'name_last' => null,
                'session_id' => null,
                'email' => null
            ];

        return view('request-password', $data);
    }

    public function requestPasswordAction(Request $request)
    {
        $client = new Client(['base_uri' => 'http://api.signin.ks']);
        $response = $client->request('POST',
            '/api/v1/user/request-password-reset',
            [
                'form_params' => [
                    'email' => $request->input('email')
                ],
                'http_errors' => false
            ]);
        switch ($response->getStatusCode()) {
            case 200:
                Session::put('redirect', [
                    'from' => '/sign-in',
                    'to' => '/sign-in',
                    'code' => 200,
                    'message' => 'Запрос на восстановление пароля отправлен Вам на email!'
                ]);
                header('Location: /redirect');
                break;
            case 403:
                Session::put('redirect', [
                    'from' => '/sign-in',
                    'to' => '/sign-in',
                    'code' => 403,
                    'message' => 'Такой email не зарегистрирован!'
                ]);
                header('Location: /redirect');
                break;
        }
    }

    public function updateCurrentUser()
    {
        $user = Session::get('user');

        if ($user === null) {
            $user = [
                'name_first' => null,
                'name_last' => null,
                'session_id' => null,
                'email' => null,
            ];
            return view('updateCurrentUser', $user);
        }

        $client = new Client(['base_uri' => 'http://api.cabinet.ks']);
        $currentUser['currentUser'] = json_decode($client->request('GET',
            '/api/v1/user/me', [
                'http_errors' => false,
                'headers' => [
                    'X-SESSION-ID' => $user['session_id']
                ]
            ])->getBody()->getContents(), true);
        $data = $user + $currentUser;

        return view('updateCurrentUser', $data);
    }

    public function updateCurrentUserAction(Request $request)
    {
        $session_id = Session::get('user')['session_id'];

        $client = new Client(['base_uri' => 'http://api.cabinet.ks']);

        $response = $client->request('POST',
            'api/v1/user/me', [
                'http_errors' => false,
                'headers' => [
                    'x-session-id' => $session_id
                ],
                'form_params' => [
                    'first_name' => $request->input('first_name'),
                    'last_name' => $request->input('last_name')
                ]
            ]
        );

        Session::put('redirect', [
            'from' => $_SERVER['HTTP_REFERER'],
            'to' => $_SERVER['HTTP_REFERER'],
            'code' => $response->getStatusCode(),
            'message' => $response->getBody()->getContents()
        ]);
        header('Location: /redirect');
    }

    public function setPassword(Request $request)
    {
        $password = $request->input('password');

        $session_id = Session::get('user')['session_id'];

        $client = new Client(['base_uri' => 'http://api.signin.ks']);

        $response = $client->request('POST',
            'api/v1/user/set-password', [
                'http_errors' => false,
                'headers' => [
                    'x-session-id' => $session_id
                ],
                'form_params' => [
                    'password' => $password
                ]
            ]
        );

        Session::put('redirect', [
            'from' => $_SERVER['HTTP_REFERER'],
            'to' => $_SERVER['HTTP_REFERER'],
            'code' => $response->getStatusCode(),
            'message' => $response->getBody()->getContents()
        ]);
        header('Location: /redirect');
    }

    public function addEmail(Request $request)
    {
        $email = $request->input('email');

        $session_id = Session::get('user')['session_id'];

        $client = new Client(['base_uri' => 'http://api.cabinet.ks']);

        $response = $client->request('POST',
            'api/v1/user/addEmail', [
                'http_errors' => false,
                'headers' => [
                    'x-session-id' => $session_id
                ],
                'form_params' => [
                    'email' => $email
                ]
            ]
        );

        Session::put('redirect', [
            'from' => $_SERVER['HTTP_REFERER'],
            'to' => $_SERVER['HTTP_REFERER'],
            'code' => $response->getStatusCode(),
            'message' => $response->getBody()->getContents()
        ]);
        header('Location: /redirect');
    }
}
