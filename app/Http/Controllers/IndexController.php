<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Session;

/**
 * Class IndexController
 * @package App\Http\Controllers
 */
class IndexController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = Session::get('user');
        if ($data === null)
            $data = [
                'name_first' => null,
                'name_last' => null,
                'session_id' => null,
                'email' => null
            ];

        return view('welcome', $data);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function signIn()
    {
        $data = Session::get('user');
        if ($data === null) {
            $data = [
                'name_first' => null,
                'name_last' => null,
                'session_id' => null,
                'email' => null
            ];
        } else {
            header('Location: /');
        }

        return view('signin', $data);
    }

    /**
     * @param Request $request
     */
    public function signInAction(Request $request)
    {
        $client = new Client(['base_uri' => 'http://api.signin.ks/']);
        $response = $client->request('POST',
            '/api/v1/user/auth',
            [
                'form_params' => [
                    'email' => $request->input('email'),
                    'password' => $request->input('password')
                ],
                'http_errors' => false
            ]);
        switch ($response->getStatusCode()) {
            case 200:
                $loggedUser = json_decode($response->getBody()->getContents(), true);
                Session::put('user', $loggedUser);
                Session::put('redirect', [
                    'from' => '/sign-in',
                    'to' => '/',
                    'code' => 200,
                    'message' => 'Спасибо, что Вы с нами!'
                ]);
                header('Location: /redirect');
                break;
            case 302:
                Session::put('redirect', [
                    'from' => '/sign-in',
                    'to' => '/',
                    'code' => 302,
                    'message' => 'Вы уже вошли!'
                ]);
                header('Location: /redirect');
            case 400:
                Session::put('redirect', [
                    'from' => '/sign-in',
                    'to' => '/sign-in',
                    'code' => 400,
                    'message' => 'Вы ввели неправильные значения для пароля или email! Попробуйте еще раз!'
                ]);
                header('Location: /redirect');
                break;
            case 403:
                Session::put('redirect', [
                    'from' => '/sign-in',
                    'to' => '/sign-in',
                    'code' => 403,
                    'message' => 'Неправильный email или пароль! Попробуйте еще раз!'
                ]);
                header('Location: /redirect');
                break;
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function signUp()
    {
        $data = Session::get('user');
        if ($data === null) {
            $data = [
                'name_first' => null,
                'name_last' => null,
                'session_id' => null,
                'email' => null
            ];
        } else {
            header('Location: /');
        }

        return view('signup', $data);
    }

    /**
     * @param Request $request
     */
    public function signUpAction(Request $request)
    {
        $client = new Client(['base_uri' => 'http://api.signup.ks/']);
        $response = $client->request('POST',
            '/api/v1/user/sign-up',
            [
                'form_params' => [
                    'first_name' => $request->input('firstname'),
                    'last_name' => $request->input('lastname'),
                    'email' => $request->input('email'),
                    'password' => $request->input('password')
                ],
                'http_errors' => false
            ]);
        switch ($response->getStatusCode()) {
            case 200:
                Session::put('redirect', [
                    'from' => '/sign-up',
                    'to' => '/',
                    'code' => 200,
                    'message' => 'Спасибо за регистрацию! Теперь Вы можете войти со своими данными...'
                ]);
                header('Location: /redirect');
                break;
            case 302:
                Session::put('redirect', [
                    'from' => '/sign-up',
                    'to' => '/',
                    'code' => 302,
                    'message' => 'Пользователь с таким email уже существует!'
                ]);
                header('Location: /redirect');
                break;
            case 400:
                Session::put('redirect', [
                    'from' => '/sign-up',
                    'to' => '/',
                    'code' => 400,
                    'message' => 'Неверно введены данные!'
                ]);
                header('Location: /redirect');
                break;
        }
    }

    /**
     * @param Request $request
     */
    public function signOutAction(Request $request)
    {
        $client = new Client(['base_uri' => 'http://api.signin.ks/']);
        $response = $client->request('POST',
            '/api/v1/session/expire', [
                'headers' => [
                    'X-SESSION-ID' => Session::get('user')['session_id']
                ],
                'http_errors' => false
            ]);
        switch ($response->getStatusCode()) {
            case 200:
                Session::forget('user');
                Session::put('redirect', [
                    'from' => $_SERVER['HTTP_REFERER'],
                    'to' => '/',
                    'code' => 200,
                    'message' => 'Вы успешно вышли!'
                ]);
                header('Location: /redirect');
                break;
            case 401:
                Session::put('redirect', [
                    'from' => $_SERVER['HTTP_REFERER'],
                    'to' => '/',
                    'code' => 401,
                    'message' => 'Перед тем как выйти необходимо авторизироваться!'
                ]);
                Session::forget('user');
                header('Location: /redirect');
                break;
            case 403:
                Session::put('redirect', [
                    'from' => $_SERVER['HTTP_REFERER'],
                    'to' => '/',
                    'code' => 403,
                    'message' => 'Неправильный токен авторизации!'
                ]);
                Session::forget('user');
                header('Location: /redirect');
                break;
        }
    }

    public function setNewPassword($token)
    {
        $data = [
            'name_first' => null,
            'name_last' => null,
            'session_id' => null,
            'email' => null
        ];

        $client = new Client(['base_uri' => 'http://api.signin.ks/']);
        $response = $client->request('POST',
            '/api/v1/user/check-request-password-token',
            [
                'form_params' => [
                    'remindToken' => $token,
                ],
                'http_errors' => false
            ]);

        switch ($response->getStatusCode()) {
            case 200:
                return view('remindPassword', $data)->with('tokenValid', true)->with('token', $token);
                break;
            case 403:
                return view('remindPassword', $data)->with('tokenValid', false);
                break;
        }
    }
    /**
     * @param Request $request
     */
    public function setNewPasswordByTokenAction(Request $request, $token)
    {
        $client = new Client(['base_uri' => 'http://api.signin.ks/']);
        $response = $client->request('POST',
            '/api/v1/user/set-password/by-token',
            [
                'form_params' => [
                    'password-token' => $token,
                    'password' => $request->input('password')
                ],
                'http_errors' => false
            ]);

        switch ($response->getStatusCode()) {
            case 200:
                Session::put('redirect', [
                    'from' => $_SERVER['HTTP_REFERER'],
                    'to' => '/sign-in',
                    'code' => 200,
                    'message' => 'Пароль успешно изменен!'
                ]);
                header('Location: /redirect');
                break;
            case 403:
                Session::put('redirect', [
                    'from' => $_SERVER['HTTP_REFERER'],
                    'to' => '/user/request-password',
                    'code' => 403,
                    'message' => 'Ошибка изменения пароля, неправильный токен!'
                ]);
                header('Location: /redirect');
                break;
        }
    }
}
