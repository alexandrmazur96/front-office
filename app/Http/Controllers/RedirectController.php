<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RedirectController extends Controller
{
    public function index(Request $request)
    {
        $data = Session::get('redirect');
        Session::forget('redirect');
        if ($data === null)
            header('Location: /');
        return view('redirect', $data);
    }
}
