@extends('layouts.auth')

@section('user-css')
    <link rel="stylesheet" href="{{ asset('css/signin.css') }}">
@endsection

@section('user-js')
    <script src="{{URL::asset('js/auth.js')}}"></script>
@endsection
@section('title', 'The Library - авторизация')

@section('content')
    <div class="container">

        <div class="row" style="margin-top:20px">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                <form role="form" method="post" name="sign_in_form" action="/sign-in" onsubmit="return validateForm();">
                    <fieldset>
                        <h2>Пожалуйста, войдите!</h2>
                        <div class="form-group">
                            <input type="email" name="email" id="email" class="form-control input-lg"
                                   placeholder="Email адрес">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" id="password" class="form-control input-lg"
                                   placeholder="Пароль">
                        </div>
                        <a href="/user/request-password" class="btn btn-link">Забыли пароль?</a>
                        </span>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="Войти">
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <a href="/sign-up" class="btn btn-lg btn-primary btn-block">Регистрация</a>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection