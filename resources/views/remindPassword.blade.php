@extends('layouts.main')

@section('user-css')
    <link rel="stylesheet" href="{{ asset('css/remindPassword.css') }}">
@endsection

@section('user-js')
    <script src="{{ asset('js/remindPassword.js') }}"></script>
@endsection
@section('title', 'The Library - восстановление пароля')
@section('content')
    @if(!$tokenValid)
        <div class="container-fluid">
            <div class="row">
                <h3>Ссылка не правильна или не действительна!</h3>
            </div>
        </div>
    @else
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <h3 class="text-center">Установите новый пароль</h3>
                    <hr/>
                    <form action="/user/set-new-password/{{ $token }}" method="post" id="form"
                          onsubmit="return validateForm();">
                        <div class="form-group">
                            <label>Пароль:</label>
                            <input type="password" id="password" name="password" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Подтвердите пароль:</label>
                            <input type="password" id="confirmPassword" class="form-control"/>
                        </div>
                        <input type="submit" class="btn btn-primary btn-sm center-block"/>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection