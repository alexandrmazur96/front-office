@extends('layouts.main')

@section('user-css')
    <link rel="stylesheet" href="{{ asset('css/sideUser.css') }}">
@endsection

@section('user-js')

@endsection

@section('title', 'The Library - просмотр данных стороннего пользователя')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @if($session_id !== null)
                <div class="col-md-4">
                    <div class="img"><img src="{{ asset('img/user.jpg') }}" alt="user img"></div>
                    <div class="type">
                        <p>
                            @if($user['is_admin'] === 1)
                                Администратор
                            @elseif($user['is_author'] === 1)
                                Автор
                            @else
                                Пользователь
                            @endif

                        </p>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="user-data">
                        <p><strong>Имя: </strong>{{ $user['first_name'] }}</p>
                        <p><strong>Фамилия: </strong>{{ $user['last_name'] }}</p>
                    </div>
                </div>
            @else
                <div class="col-md-12 text-center">
                    <h3>
                        Для просмотра текущего пользователя следует <a href="/sign-in">залогиниться!</a>
                    </h3>
                </div>
            @endif
        </div>
    </div>
@endsection