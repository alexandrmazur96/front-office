@extends ('layouts.auth')

@section('user-css')
    <link rel="stylesheet" href="{{ asset('css/request-password.css') }}">
@endsection

@section('user-js')

@endsection

@section('title', 'The Library - восстановление пароля')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                <form role="form" action="/user/request-password" method="post" class="sign_up_form">
                    <h2 class="sign_up_title">Восстановление пароля</h2>
                    <p>Введите свой email адрес и мы вышлем Вам письмо с reset link.</p>
                    <div class="form-group">
                        <input type="email" name="email" id="email" class="form-control input-lg"
                               placeholder="Email адрес" tabindex="4">
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <button class="btn btn-success btn-block btn-lg">Восстановить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection