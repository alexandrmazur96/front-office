@extends('layouts.main')

@section('user-css')
    <link rel="stylesheet" href="{{ asset('css/searchBooks.css') }}">
@endsection

@section('user-js')

@endsection

@section('title', 'The Library - поиск книги')

@section('content')
    @if($error !== null)
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>{{ $error }}</h3>
                </div>
            </div>
        </div>
    @else
        @foreach($finded as $book)
            <div class="col-md-3">
                <div class="book-block">
                    <div class="img">
                        <h5 class="text-center">{{ $book['name']}}</h5>
                        <img src="{{ asset('img/book.jpg') }}" alt="user img">
                    </div>
                    <div class="book-data">
                        <p>ISBN: {{ $book['isbn'] }}</p>
                        <p>
                            {{ str_limit($book['description'], $limit = 100, $end = '...') }}
                        </p>
                    </div>
                    <div class="book-link">
                        <a href="/book/{{ $book['id']}}">Подробнее</a>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
@endsection