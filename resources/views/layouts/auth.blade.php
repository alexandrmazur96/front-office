<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('libs/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/font-awesome/css/font-awesome.css') }}">

    @yield('user-css')

    <script src="{{ asset('libs/jquery/jquery-3.1.0.min.js') }}"></script>
    <script src="{{ asset('libs/bootstrap/bootstrap.min.js') }}"></script>

    @yield('user-js')

    <title>@yield('title')</title>
</head>
<body>

<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="text-left header-item header-logo"><a href="/">Library</a></div>
                <div class="text-right header-item header-btns">
                    @if ($name_first === null)
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle header-user-btn" type="button"
                                    data-toggle="dropdown"><i
                                        class="fa fa-user-o" aria-hidden="true"></i></button>
                            <ul class="dropdown-menu">
                                <li><a href="/sign-in"><i class="fa fa-sign-in" aria-hidden="true"></i>Авторазиция</a>
                                </li>
                                <li><a href="/sign-up"><i class="fa fa-pencil" aria-hidden="true"></i>Регистрация</a>
                                </li>
                            </ul>
                        </div>
                    @else
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle header-user-btn" type="button"
                                    data-toggle="dropdown">{{ $name_first }}</button>
                            <ul class="dropdown-menu">
                                <li><a href="/user/me"><i class="fa fa-user-circle-o" aria-hidden="true"></i>Мой
                                        профиль</a>
                                </li>
                                <li>
                                    <form action="/user/sign-out" method="post">
                                        <button type="submit" id="exitBtn">
                                            <i class="fa fa-sign-out" aria-hidden="true"></i>Выход
                                        </button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</header>
<div class="main-container">
    @yield('content')
</div>
<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="text-left">
                    <p>@ Mazur Alexandr {{ date('Y') }}</p>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>