@extends('layouts.main')

@section('user-css')
    <link rel="stylesheet" href="{{ asset('css/welcome.css') }}">
@endsection

@section('user-js')
    <script src="{{URL::asset('js/app.js')}}"></script>
@endsection
@section('title', 'The Library - главная')

@section('sidebar')

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>Добро пожаловать в библиотеку "The Library"!</h3>
            </div>
        </div>
    </div>
@endsection