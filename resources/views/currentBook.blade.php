@extends('layouts.main')

@section('user-css')
    <link rel="stylesheet" href="{{ asset('css/currentBook.css') }}">
@endsection

@section('user-js')

@endsection

@section('title', 'The Library - книга')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="book-preview">
                    <div class="title text-center">
                        <h3>{{ $info['name'] }}</h3>
                    </div>
                    <div class="img"><img src="{{ asset('img/book.jpg') }}" alt="book img"></div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="book-data">
                    <p><strong>ISBN: </strong>{{ $info['isbn'] }}</p>
                    <p><strong>Год выпуска: </strong>{{ $info['publicated'] }}</p>
                    <p><strong>Количество страниц: </strong>{{ $info['pages'] }}</p>
                    <p><strong>Формат: </strong>{{ $info['format'] }}</p>
                    <p><strong>Описание:</strong><br>{{ $info['description'] }}</p>
                    <p><strong>Авторы:</strong></p>
                    <div class="authors">
                        <p>
                            @foreach($authors as $author)
                                {{ $author['first_name'] }} {{ $author['last_name'] }}<br>
                            @endforeach
                        </p>
                    </div>
                    @if($name_first !== null)
                        <div class="buttons">
                            <form action="/book/{{ $info['id'] }}" method="POST">
                                <input class="btn btn-primary" type="submit" value="Добавить книгу"/>
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection