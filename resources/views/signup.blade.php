@extends('layouts.auth')

@section('user-css')
    <link rel="stylesheet" href="{{ asset('css/signup.css') }}">
@endsection

@section('user-js')
    <script src="{{URL::asset('js/signUp.js')}}"></script>
@endsection
@section('title', 'The Library - регистрация')

@section('content')
    <div class="container" id="wrap">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <form method="post" action="/sign-up"accept-charset="utf-8" class="form" role="form" onsubmit="return validateForm( );">
                    <legend>Регистрация</legend>
                    <h4>It's free and always will be.</h4>
                    <div class="row">
                        <div class="col-xs-6 col-md-6">
                            <input type="text" id="firstName" name="firstname" value="" class="form-control input-lg"
                                   placeholder="First Name"/></div>
                        <div class="col-xs-6 col-md-6">
                            <input type="text" id="lastName" name="lastname" value="" class="form-control input-lg"
                                   placeholder="Last Name"/></div>
                    </div>
                    <input type="text" name="email" id="email" value="" class="form-control input-lg"
                           placeholder="Your Email"/>
                    <input type="password" name="password" id="password" value=""
                           class="form-control input-lg" placeholder="Password"/>
                    <input type="password" id="confirmPassword" name="confirm_password" value="" class="form-control input-lg"
                           placeholder="Confirm Password"/>
                    <div class="row">
                        <br/>
                        <span class="help-block">Нажимая "Создать Аккаунт", Вы соглашаетесь с нашими правилами, вклячая наши Cookie Use.</span>
                        <button class="btn btn-lg btn-primary btn-block signup-btn" type="submit">
                            Создать аккаунт
                        </button>
                </form>
            </div>
        </div>
    </div>

@endsection