@extends('layouts.main')

@section('user-css')
    <link rel="stylesheet" href="{{ asset('css/allBooks.css') }}">
@endsection

@section('user-js')
    <script src="{{ asset('js/search.js') }}"></script>
@endsection

@section('title', 'The Library - все книги')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div id="custom-search-input">
                    <div class="input-group col-md-12">
                        <input type="text" class="form-control input-lg" id="searchInput"
                               placeholder="Type search term..."/>
                        <span class="input-group-btn">
                            <a class="btn btn-info btn-lg" id="searchBtn" href="javascript:void(0);">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($books as $book)
                <div class="col-md-4">
                    <div class="book-block">
                        <div class="img">
                            <h5 class="text-center">{{ $book->name }}</h5>
                            <img src="{{ asset('img/book.jpg') }}" alt="user img">
                        </div>
                        <div class="book-data">
                            <p>ISBN: {{ $book->isbn }}</p>
                            <p>
                                {{ str_limit($book->description, $limit = 100, $end = '...') }}
                            </p>
                        </div>
                        <div class="book-link">
                            <a href="/book/{{ $book->id }}">Подробнее</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection