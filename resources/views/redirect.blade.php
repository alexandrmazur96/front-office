<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('libs/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/redirect.css') }}">
    <script src="{{ asset('js/redirect.js') }}"></script>
    <title>Redirecting...</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center">
            <div class="message">
                <h3>{{ $message}}</h3>
                <p>Вы будете перенаправлены через <span id="redirect-count"></span></p>
                <p>или <a href="{{ $to }}" id="to">нажмите</a>, чтобы перейти немедленно.</p>
            </div>
        </div>
    </div>
</div>
</body>
</html>