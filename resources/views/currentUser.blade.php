@extends('layouts.main')

@section('user-css')
    <link rel="stylesheet" href="{{ asset('css/currentUser.css') }}">
@endsection

@section('user-js')

@endsection

@section('title', 'The Library - просмотр данных текущего пользователя')

@section('content')
    @if($session_id === null)
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>
                        Для просмотра текущего пользователя следует <a href="/sign-in">залогиниться!</a>
                    </h2>
                </div>
            </div>
        </div>
    @else
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="img">
                        <img src="{{ asset('/img/user.jpg') }}" alt="user img">
                    </div>
                    <div class="type text-center">
                        <p>
                            @if($is_admin === 1)
                                Администратор
                            @elseif($is_author === 0 && $is_admin === 0)
                                Пользователь
                            @else
                                Автор
                            @endif
                        </p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="user-data">
                        <p>Имя: {{ $currentUser['first_name'] }}</p>
                        <p>Фамилия: {{ $currentUser['last_name'] }}</p>
                        <p>
                            Email-адреса:
                            <div class="emails">
                        @foreach($currentUser['emails'] as $email)
                            <p>{{ $email }}</p>
                        @endforeach
                    </div>
                    <a href="/user/me/update">Изменения профиля</a>
                </div>
            </div>
        </div>
    @endif
@endsection