@extends('layouts.main')

@section('user-css')
    <link rel="stylesheet" href="{{ asset('css/allUsers.css') }}">
@endsection

@section('user-js')

@endsection

@section('title', 'The Library - все пользователи')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @foreach($users as $user)
                <div class="col-md-3">
                    <div class="user-block">
                        <div class="img">
                            <img src="{{ asset('img/user.jpg') }}" alt="user img">
                        </div>
                        <div class="user-data">
                            <p>{{ $user->first_name }}</p>
                            <p>{{ $user->last_name }}</p>
                            <p>
                                @if($user->is_admin === 1)
                                    Администратор
                                @else
                                    Пользователь
                                @endif
                            </p>
                        </div>
                        <div class="user-link">
                            <a href="/user/{{ $user->id }}">Подробнее</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection