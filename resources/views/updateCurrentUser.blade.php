@extends('layouts.main')

@section('user-css')
    <link rel="stylesheet" href="{{ asset('css/currentUser.css') }}">
@endsection

@section('user-js')
    <script src="{{ asset('js/updateProfile.js') }}"></script>
@endsection

@section('title', 'The Library - просмотр данных текущего пользователя')

@section('content')
    @if($session_id === null)
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>
                        Для просмотра текущего пользователя следует <a href="/sign-in">залогиниться!</a>
                    </h2>
                </div>
            </div>
        </div>
    @else
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="img">
                        <img src="{{ asset('/img/user.jpg') }}" alt="user img">
                    </div>
                    <div class="type text-center">
                        <p>
                            @if($is_admin === 1)
                                Администратор
                            @elseif($is_author === 0 && $is_admin === 0)
                                Пользователь
                            @else
                                Автор
                            @endif
                        </p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="user-data">
                        <form action="/user/me/update" method="post" onsubmit="return validateForm();">
                            <div class="form-group">
                                <h4 class="text-center">Смена личных данный</h4>
                                <label for="firstNameIn"><strong>Имя: </strong></label>
                                <input type="text" id="firstNameIn" class="form-control" name="first_name"
                                       placeholder="Введите имя..."
                                       value="{{ $currentUser['first_name'] }}">
                                <label for="lastNameIn"><strong>Фамилия: </strong></label>
                                <input type="text" id="lastNameIn" class="form-control" name="last_name"
                                       placeholder="Введите фамилию..."
                                       value="{{ $currentUser['last_name'] }}">
                                <input type="submit" class="btn btn-primary" value="Изменить">
                            </div>
                        </form>
                        <form action="/user/me/set-password" method="post" class="changePassword"
                              onsubmit="return validatePasswords();">
                            <div class="form-group">
                                <h4 class="text-center">Смена пароля</h4>
                                <label for="newPswIn">Новый пароль:</label>
                                <input type="password" class="form-control" id="newPswIn" name="password"
                                       placeholder="Type new password">
                                <input type="submit" class="btn btn-primary" value="Изменить">
                            </div>
                        </form>
                        <form action="/user/me/add-email" method="post" onsubmit="return validateEmail()">
                            <div class="form-group">
                                <h4 class="text-center">Добавления email</h4>
                                <label for="addEmailIn"><strong>Email:</strong></label>
                                <input type="text" name="email" id="addEmailIn" class="form-control"
                                       placeholder="Введите новый email">
                                <input type="submit" class="btn btn-primary" value="Добавить">
                            </div>
                        </form>

                        <p>
                            Email-адреса:
                            <div class="emails">
                        @foreach($currentUser['emails'] as $email)
                            <p>{{ $email }}</p>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection