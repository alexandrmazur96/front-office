<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use \Illuminate\Support\Facades\Route;

$router->group(['prefix' => '/'], function ($router) {
    $router->get('/', 'IndexController@index');
    $router->get('/sign-in', 'IndexController@signIn');
    $router->post('/sign-in', 'IndexController@signInAction');
    $router->get('/sign-up', 'IndexController@signUp');
    $router->post('/sign-up', 'IndexController@signUpAction');
    $router->get('/users', 'UsersController@allUsers');
    $router->get('/books', 'BooksController@allBooks');
    $router->get('/redirect', 'RedirectController@index');
});

Route::group(['prefix' => '/user'], function () {
    Route::get('/me', 'UsersController@currentUser');
    Route::get('/me/update', 'UsersController@updateCurrentUser');
    Route::post('/me/add-email', 'UsersController@addEmail');
    Route::post('/me/update', 'UsersController@updateCurrentUserAction');
    Route::post('/me/set-password', 'UsersController@setPassword');
    Route::get('/set-new-password/{token}', 'IndexController@setNewPassword');
//        ->where('token', 'a-zA-Zа-яА-Я\d{32,}');
    Route::post('/set-new-password/{token}', 'IndexController@setNewPasswordByTokenAction');
//        ->where('token', 'a-zA-Zа-яА-Я\d{32,}');
    Route::get('/request-password', 'UsersController@requestPassword');
    Route::post('/request-password', 'UsersController@requestPasswordAction');
    Route::get('/{userId}', 'UsersController@sideUser')->where('userId', '\d+');
    Route::post('/sign-out', 'IndexController@signOutAction');
});


$router->group(['prefix' => '/book'], function ($router) {
    $router->get('/me', 'BooksController@currentUserBooks');
    $router->get('/{bookId}', 'BooksController@currentBook')->where('bookId', '\d+');
    $router->post('/{bookId}', 'BooksController@addCurrentBook')->where('bookId', '\d+');
    $router->get('/user/{userId}', 'BooksController@sideUserBooks')->where('userId', '\d+');
    $router->get('/author/{authorId}', 'BooksController@authorBooks')->where('authorId', '\d+');
    $router->get('/search/{term}', 'BooksController@searchBooks');
//        ->where('term', 'a-zA-Zа-яА-Я\d{3,}');
});
