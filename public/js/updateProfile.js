function validateEmail() {
    var email = document.getElementById('addEmailIn').value;
    var valid = true;
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    valid = re.test(email);
    if (!valid)
        alert('Bad email!');
    return valid;
}

function validateForm() {
    var fname = document.getElementById('firstNameIn').value;
    var lname = document.getElementById('lastNameIn').value;
    var message = '';
    var valid = true;
    if (fname.length < 3) {
        valid = false;
        message += 'Имя слишком короткое!\n';
    }

    if (lname.length < 3) {
        valid = false;
        message += 'Фамилия слишком короткая!\n';
    }

    if (!valid)
        alert(message);

    return valid;
}

function validatePasswords() {
    var npass = document.getElementById('newPswIn').value;
    var valid = true;
    var message = '';

    if (npass.length < 8) {
        valid = false;
        message += 'Новый пароль слишком короткий!\n';
    }

    if (!valid)
        alert(message);

    return valid;
}