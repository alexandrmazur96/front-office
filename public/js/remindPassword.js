function validateForm() {
    var valid = true;
    var password = document.getElementById('password').value;
    var confirmPassword = document.getElementById('confirmPassword').value;

    var message = '';

    if (password.length < 8) {
        valid = false;
        message += '\nПароль слишком короткий!';
    }

    if (password !== confirmPassword) {
        valid = false;
        message += '\nПароли не совпадают!';
    }

    if (!valid)
        alert(message);

    return valid;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return re.test(email);
}