function validateForm() {
    var valid = true;
    var firstName = document.getElementById('firstName').value;
    var lastName = document.getElementById('lastName').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    var confirmPassword = document.getElementById('confirmPassword').value;

    var message = '';

    if (firstName.length < 3) {
        valid = false;
        message += '\nИмя слишком короткое!';
    }

    if (lastName.length < 3) {
        valid = false;
        message += '\nФамилия слишком короткая!';
    }

    if (!validateEmail(email)) {
        valid = false;
        message += '\nНеправильный email!';
    }

    if (password.length < 8) {
        valid = false;
        message += '\nПароль слишком короткий!';
    }

    if (password !== confirmPassword) {
        valid = false;
        message += '\nПароли не совпадают!';
    }

    if (!valid)
        alert(message);

    return valid;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return re.test(email);
}