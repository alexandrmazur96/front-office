window.onload = function () {
    i = 3;
    counter();
}

function counter() {
    if (i <= 0) {
        redirect();
        return;
    }
    i--;
    timer = setTimeout('counter()', 1000);
    document.getElementById('redirect-count').innerHTML = i;
}

function redirect() {
    document.location.href = 'http://' +
        document.domain +
        ':8084' +
        document.getElementById('to').getAttribute('href');
}