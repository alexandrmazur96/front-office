function validateForm() {
    var valid = true;
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    var message = '';
    if (isEmptyOrSpaces(email) && !validateEmail(email)) {
        valid = false;
        message += '\nПустой или неправильный email!';
    }

    if (password.length < 8) {
        valid = false;
        message += '\nСлишком короткий пароль!';
    }

    if (!valid)
        alert(message);

    return valid;
}

function isEmptyOrSpaces(str) {
    return str === null || str.match(/^ *$/) !== null;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return re.test(email);
}